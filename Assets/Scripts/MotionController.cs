﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionController : MonoBehaviour {

    
    [SerializeField]
    private float MaxZoomFactor = 60f;
    [SerializeField]
    private float MinZoomFactor = 1f;
    [SerializeField]
    private float ZoomStep = 10f;
    [Space(10)]
    [SerializeField]
    private float RotationSpeed = 10f;
    //[SerializeField]
  //  private bool Inertial = false;

    public Transform Target;

    [Space(10)]
    public bool StepMode = false;
    public bool MathCameraDirection = true;

    private Transform _camera;
    private Quaternion _target;
    private Vector3 _defaultSize;
    private float _zoomTarget = 0;

    void Start () {

        _camera = GameObject.Find("ARCamera").transform;
        _defaultSize = Target.localScale ;
        _zoomTarget = 1f;
        _target = Target.localRotation;

	}

    int _zoomDir = 0,
           _rotDir = 0;
    float _targetAngle = 0;
	// Update is called once per frame
	void Update () {

        if (!StepMode)
        {
            if (_zoomDir != 0)
            {
                _zoomTarget += System.Math.Sign(_zoomDir) * ZoomStep * Time.deltaTime;
                _zoomTarget = Mathf.Clamp(_zoomTarget, MinZoomFactor, MaxZoomFactor);
            }

            if (_rotDir != 0)
            {
                var dir = 1.0f;
                if (MathCameraDirection)
                    dir = _camera.transform.up.y < 0 ? -1 : 1;


                _targetAngle += System.Math.Sign(_rotDir) * RotationSpeed * dir;
                _target = Quaternion.AngleAxis(_targetAngle, Vector3.up);
            }
        }

        if (Target.gameObject.activeSelf)
        {
            Target.localScale = Vector3.MoveTowards(Target.localScale, _zoomTarget * _defaultSize, Time.deltaTime * ZoomStep);
            Target.localRotation = Quaternion.Slerp(Target.localRotation, _target, Time.deltaTime * RotationSpeed * 10f);
        } else
        {
            _zoomDir = 0;
            _rotDir = 0;
        }
	}

    public void OnPointerExit()
    {
        _zoomDir = 0;
        _rotDir = 0;
    }
    public void OnZoomGoBack()
    {
        _zoomTarget = 1f;
    }

    public void OnZoomPressed(int dir)
    {
        if (!StepMode)
        {
            _zoomDir = dir ;
        } else
        {
            _zoomTarget += Mathf.Sign(dir) * ZoomStep;
        }

        _zoomTarget = Mathf.Clamp(_zoomTarget, MinZoomFactor, MaxZoomFactor);
    }

    public void OnRotatePressed(int dir)
    {
        if (!StepMode)
        {
            _rotDir = dir;
        } else
        {
            _targetAngle += RotationSpeed * Mathf.Sign(dir);
        }
    }
}
