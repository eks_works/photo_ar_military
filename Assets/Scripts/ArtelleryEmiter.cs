﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArtelleryEmiter : MonoBehaviour {

    public AudioClip clip;
    public ParticleSystem parts;
    public int count = 20;

    AudioSource _source;

    private void Start()
    {
        _source = GetComponent<AudioSource>();
    }

    public void Shot()
    {
        if(_source != null && clip != null )
            _source.PlayOneShot(clip);

        parts.Emit(count);
    }
}
