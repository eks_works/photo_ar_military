﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;
public class UIcontroller : MonoBehaviour {

    public GameObject TargetUI;
    public GameObject DarkBackground;
    public GameObject DisabledUI;
    public GameObject EnabledUI;
    public GameObject ShareUI;
    public Text _debug;
    public RawImage ScreenshotImageTarget;
    public Animator ScreenshotAnimator;
    public AudioClip TakeScreeenClip;
    public Vector2 RefResolution = new Vector2(1920, 1080);
    public bool RecalculateSize = true;
    [Space(10)]
    [Multiline]
    public string[] ShareTexts = { "WhiteTower eks.works ", "WhiteTower eks.works " };
    [Multiline]
    public string[] ShareSubjects = { "http://eksworks.com", "" };
    [Multiline]
    public string ShareURLiOS = "http://eksworks.com";


    private AudioSource _audio;
    private CanvasScaler _scaler;
    private NativeShare _native;
    void Start()
    {

        if (Application.platform == RuntimePlatform.WindowsEditor)
            RecalculateSize = false;
        _native = GetComponent<NativeShare>();
        _audio = GameObject.FindObjectOfType<AudioSource>();

        ScreenshotManager.OnScreenshotTaken += OnTakeTexture;
        ScreenshotManager.OnScreenshotSaved += OnImageSaved;


        DeviceChanged.OnOrientationChange += OnOrientationChanged;
        _scaler = GetComponent<CanvasScaler>();

        OnOrientationChanged(Input.deviceOrientation);

        DefaultTrackableEventHandler.IsTracckerFind +=(IsTrackerFind);
        DefaultTrackableEventHandler.IsTracckerLost +=(IsTrackerLost);

    }


    void IsTrackerLost()
    {
        TargetUI.SetActive(true);
    }

    void IsTrackerFind()
    {
        TargetUI.SetActive(false);
    }

    void OnOrientationChanged(DeviceOrientation _orient)
    {
        if (!RecalculateSize)
            return;
        if (_orient == DeviceOrientation.LandscapeLeft || _orient == DeviceOrientation.LandscapeRight)
            _scaler.referenceResolution = RefResolution;
        else
            _scaler.referenceResolution = new Vector2(RefResolution.y, RefResolution.x);
        
    }

    private void OnApplicationPause(bool pause)
    {
        OnOrientationChanged(Input.deviceOrientation);
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        
    }

    public void SnapShot()
    {
        ScreenshotManager.SaveScreenshot("WhiteTower", "WhiteTowerAR");

        DisabledUI.SetActive(false);
        if(EnabledUI != null)
            EnabledUI.SetActive(true);

        if (TakeScreeenClip != null)
            _audio.PlayOneShot(TakeScreeenClip);

    }


    public void OnTakeTexture(Texture2D _tex)
    {
        //DisabledUI.SetActive(true);
        if(EnabledUI != null)
            EnabledUI.SetActive(false);
        if (ScreenshotAnimator != null && ScreenshotImageTarget != null)
        {
            ScreenshotImageTarget.texture = _tex;
            ScreenshotAnimator.SetBool("IsShow", true);
            ShareUI.SetActive(true);
            if (DarkBackground != null)
                DarkBackground.SetActive(true);
        } else
        {
            DisabledUI.SetActive(true);
        }
    }

    private string share_path = null;
    public void OnClickShare()
    {

        ShareUI.SetActive(false);
        if (EnabledUI != null)
            EnabledUI.SetActive(false);


        if (DarkBackground != null)
            DarkBackground.SetActive(false);

        DisabledUI.SetActive(true);
        ScreenshotAnimator.SetBool("IsShow", false);

        if (share_path == null)
        {
            return;
        }
        
        int index =Application.platform == RuntimePlatform.Android ? 0 : 1;

        _native.Share(ShareTexts[index], share_path, ShareURLiOS, ShareSubjects[index]);

    }
    
    
    public void OnClickClose()
    {
        share_path = null;
        OnClickShare();
    }

    
    public void OnImageSaved(string path)
    {
        if (path == null || path == "DENIED" || path == "TIMEOUT")
        {
            share_path = null;
        }

        share_path = path;
        if(_debug != null)
            _debug.text = path;
    }
    public void OpenURLFromText(Text text)
    {
        OpenURL(text.text);
    }
    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }
}
