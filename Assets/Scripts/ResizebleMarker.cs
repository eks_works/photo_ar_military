﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResizebleMarker : MonoBehaviour {

    public RectTransform ParentTransform, CanvasRect;
    private RectTransform _canvasRect, _rect;

    void Start () {
        
        _rect = GetComponent<RectTransform>();

        DeviceChanged.OnOrientationChange += OnRotate;

       // gameObject.SetActive(false);
	}

    private Vector2 _targetSize, _lastSize;
    private Vector2 _targetPos, _lastPos;

    private bool _lerp = false;
    private bool _onSized = false;
    private bool _scaleUp = false;
    private float _lerpTime = 0;

    void Update () {

        if (_lerp)
        {
            _rect.anchoredPosition = Vector2.Lerp(_lastPos, _targetPos, _lerpTime);
            _rect.sizeDelta = Vector2.Lerp(_lastSize, _targetSize, _lerpTime);
            _lerpTime += Time.deltaTime * 5f;

            if(_lerpTime >=1f)
            {
                _lerp = false;
                if (!_scaleUp)
                    gameObject.SetActive(false);
            }
        }    
	}

    public void OnRotate(DeviceOrientation or)
    {
        // OnScaleDown();

        gameObject.SetActive(false);
    }
    public void OnEnable()
    {
        if (_rect == null)
            Start();
        _rect.anchoredPosition = ParentTransform.anchoredPosition;
    }

    public void OnScaleUp()
    {
        gameObject.SetActive(true);
        _targetSize = CanvasRect.sizeDelta;
        _targetPos = Vector2.zero; //CanvasRect.sizeDelta * 0.5f;
        _targetPos.y *= -1;
        _lerp = true;
        _lerpTime = 0;
        _scaleUp = true;
        _lastSize = _rect.sizeDelta;
        _lastPos = _rect.anchoredPosition;

        _onSized = true;
    }

    public void OnScaleDown()
    {
       // gameObject.SetActive(true);
        _targetSize = ParentTransform.sizeDelta;
        _targetPos = ParentTransform.anchoredPosition;
        _lerp = true;
        _lerpTime = 0;
        _scaleUp = false;

        _lastSize = _rect.sizeDelta;
        _lastPos = _rect.anchoredPosition;

        _onSized = false;

    }
}
