﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class Autofocus : MonoBehaviour {

    public float RefocusTime;
    public CameraDevice.FocusMode mode = CameraDevice.FocusMode.FOCUS_MODE_NORMAL;
    public bool InvokeFocus = false;
    public bool Reinvoke = false;

    void Start() {


        DefaultTrackableEventHandler.IsTracckerLost += () => { InvokeFocus = true; StartCoroutine(Focus()); };
        DefaultTrackableEventHandler.IsTracckerFind += () => { InvokeFocus = false; };

      
        CameraDevice.Instance.SetFocusMode(mode);
    }

    IEnumerator Focus()
    {
        while (InvokeFocus && Reinvoke)
        {
            CameraDevice.Instance.SetFocusMode(mode);
            yield return new WaitForSeconds(RefocusTime);
        }

    }

    public void ManualFocus()
    {
        CameraDevice.Instance.SetFocusMode(mode);
    }
}
